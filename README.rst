Nglimsclient
======
nglims client package for bioblend


Installation
------------

You can install the latest stable version from PyPI

.. code:: console

    $ pip install nglimsclient